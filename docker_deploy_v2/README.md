# GOBii Keycloak Docker deployment

Given the necessary environment variables, this script (`keycloak-run.sh`) will
use Docker to configure and run a Keycloak container and a database container.

For persistence: the database data is stored outside of Docker, on the host,
in the directory: `$BUNDLE_PARENT_PATH/gobii-keycloak/pg_data`.

## Configuration

This uses the following environment variables, provided by a `gobii.parameters` file or Jenkins.

1. `BUNDLE_PARENT_PATH`           (str) absolute path to your persistant data on the host (usually `/data`)
2. `KEYCLOAK_REALM`               (str) name of realm (used for health check)
3. `KEYCLOAK_ADMIN_USERNAME`      (str) initial admin user for Keycloak administration UI
4. `KEYCLOAK_ADMIN_PASSWORD`      (str) initial password for the above
5. `KEYCLOAK_DB_ADMIN_USERNAME`   (str) initial database username
6. `KEYCLOAK_DB_ADMIN_PASSWORD`   (str) initial database user password
7. `KEYCLOAK_URL_BASE`            (str) URL that Keycloak is deployed to (`http://localhost/keycloak/auth`)

If you do not have an existing Keycloak instance setup, the installation script will import a sample realm.
The realm provides the necessary roles, groups, clients, etc that are needed to work with the GDM software.
The name of the realm will be "Gobii" during the initial install, but can be changed after, if desired.

## Importing an existing realm

During an upgrade or due to a transition to another server, you may already have a realm configured.

If importing an existing realm, before running this script you will need to copy that to `BUNDLE_PARENT_PATH/realm_data.json` (`/data` normally).
The installation script will pick this up and import your realm into the new instance of Keycloak.

## Importing users from GDM

The `gobii.deploy.bash` repository has a bash script that can connect to your GDM database and import users and their crop roles into Keycloak.  The code and documentation for this script can be found in that repo at `/utils/keycloak_user_import/readme.md`.

## Running

`keycloack-run.sh` can be ran on its own, from its directory, as long as its required environment variables are set.
