#!/bin/bash
echo -e '\nStarting Keycloak installation.'

# These define what docker hub image version tags to deploy & run.
# Change these as needed to use newer versions of Keycloak or Postgres.
KEYCLOAK_IMAGE_TAG='12.0.4'
POSTGRES_IMAGE_TAG='13-alpine'

# These are left here for easy local development.
# BUNDLE_PARENT_PATH=/data
# KEYCLOAK_ADMIN_PASSWORD=adminx
# KEYCLOAK_ADMIN_USERNAME=adminx
# KEYCLOAK_DB_ADMIN_USERNAME=admin
# KEYCLOAK_DB_ADMIN_PASSWORD=admin
# KEYCLOAK_REALM=Gobii
# KEYCLOAK_URL_BASE=http://localhost/keycloak/auth

set -e
set -u
# set -v

# cd ~/workspace/gobii.deploy.keycloak/docker_deploy_v2

echo -e '\n- creating necessary directories...'
mkdir -p "$BUNDLE_PARENT_PATH"/gobii-keycloak/pg_data
mkdir -p "$BUNDLE_PARENT_PATH"/gobii-keycloak/realm_data

if test -f "$BUNDLE_PARENT_PATH/realm_data.json"; then
    echo -e "\n- Existing realm data found.  Copying to $BUNDLE_PARENT_PATH/gobii-keycloak/realm_data/realm_data.json..."
    REALM_DATA_FILE=$BUNDLE_PARENT_PATH/realm_data.json
else
    echo -e "\n- No realm data found.  Using sample_realm_data.json instead.  If you have a realm export you'd like to use, put it at $BUNDLE_PARENT_PATH/realm_data.json and run this again."
    REALM_DATA_FILE=$PWD/sample_realm_data.json
fi

command cp "$REALM_DATA_FILE" "$BUNDLE_PARENT_PATH"/gobii-keycloak/realm_data/

echo -e '\n- creating/updating Keycloak theme...'
command cp -ru ./gobii-keycloak-theme "$BUNDLE_PARENT_PATH"/gobii-keycloak/

if docker network inspect gobii-keycloak-network >/dev/null; then
    echo -e '\n- necessary docker network found.'
else
    echo -e '\n- creating necessary docker network...'
    docker network create gobii-keycloak-network
fi

if docker inspect gobii-keycloak-db >/dev/null; then
    echo -e '\n- removing old gobii-keycloak-db container...'
    docker rm -f gobii-keycloak-db
fi

echo -e '\n- checking for new database image...'
docker pull postgres:$POSTGRES_IMAGE_TAG

echo -e '\n- running new gobii-keycloak-db container...'

docker run -d --name gobii-keycloak-db \
    --health-cmd='pg_isready -U postgres || exit 1' \
    --net gobii-keycloak-network \
    --restart=always \
    -e POSTGRES_DB=keycloak_db \
    -e POSTGRES_PASSWORD="$KEYCLOAK_DB_ADMIN_PASSWORD" \
    -e POSTGRES_USER="$KEYCLOAK_DB_ADMIN_USERNAME" \
    -h gobii-keycloak-db \
    -v "$BUNDLE_PARENT_PATH"/gobii-keycloak/pg_data:/var/lib/postgresql/data \
    postgres:$POSTGRES_IMAGE_TAG

if docker inspect gobii-keycloak >/dev/null; then
    echo -e '\n- removing old gobii-keycloak container...'
    docker rm -f gobii-keycloak
fi

echo -e '\n- checking for new keycloak image...'
docker pull jboss/keycloak:$KEYCLOAK_IMAGE_TAG

echo -e '\n- running new gobii-keycloak container...'
docker run -d --name gobii-keycloak \
    --health-cmd="curl -fsS http://localhost:8080/keycloak/auth/realms/${KEYCLOAK_REALM} > /dev/null" \
    --net gobii-keycloak-network \
    --restart=always \
    -e DB_ADDR=gobii-keycloak-db \
    -e DB_DATABASE=keycloak_db \
    -e DB_PASSWORD="$KEYCLOAK_DB_ADMIN_PASSWORD" \
    -e DB_USER="$KEYCLOAK_DB_ADMIN_USERNAME" \
    -e DB_VENDOR=postgres \
    -e KEYCLOAK_FRONTEND_URL="$KEYCLOAK_URL_BASE" \
    -e KEYCLOAK_IMPORT=/tmp/realm_data/realm_data.json \
    -e PROXY_ADDRESS_FORWARDING=true \
    -h gobii-keycloak \
    -v "$BUNDLE_PARENT_PATH"/gobii-keycloak/gobii-keycloak-theme:/opt/jboss/keycloak/themes/gobii-keycloak-theme \
    -v "$BUNDLE_PARENT_PATH"/gobii-keycloak/realm_data/:/tmp/realm_data/ \
    jboss/keycloak:$KEYCLOAK_IMAGE_TAG

echo -e "\n- waiting to create admin user (30s)..."
sleep 30

docker exec gobii-keycloak /opt/jboss/keycloak/bin/add-user-keycloak.sh -u "$KEYCLOAK_ADMIN_USERNAME" -p "$KEYCLOAK_ADMIN_PASSWORD"

echo -e "\n- waiting for restart to do post-install configuration (30s)..."
docker restart gobii-keycloak
sleep 30

docker exec gobii-keycloak bash -c "
./opt/jboss/keycloak/bin/kcadm.sh config credentials \
--server http://localhost:8080/auth \
--realm master \
--user $KEYCLOAK_ADMIN_USERNAME \
--password $KEYCLOAK_ADMIN_PASSWORD;
./opt/jboss/keycloak/bin/kcadm.sh update \
realms/master \
-s sslRequired=NONE"

docker exec gobii-keycloak bash -c "sed -i -e 's/<web-context>auth<\/web-context>/<web-context>keycloak\/auth<\/web-context>/' /opt/jboss/keycloak/domain/configuration/domain.xml"
docker exec gobii-keycloak bash -c "sed -i -e 's/<web-context>auth<\/web-context>/<web-context>keycloak\/auth<\/web-context>/' /opt/jboss/keycloak/standalone/configuration/standalone.xml"
docker exec gobii-keycloak bash -c "sed -i -e 's/name=\"\/\"/name=\"\/keycloak\/\"/' /opt/jboss/keycloak/standalone/configuration/standalone.xml"
docker exec gobii-keycloak bash -c "sed -i -e 's/<web-context>auth<\/web-context>/<web-context>keycloak\/auth<\/web-context>/' /opt/jboss/keycloak/standalone/configuration/standalone-ha.xml"
docker exec gobii-keycloak bash -c "sed -i -e 's/name=\"\/\"/name=\"\/keycloak\/\"/' /opt/jboss/keycloak/standalone/configuration/standalone-ha.xml"

docker network connect gobii_net gobii-keycloak

docker restart gobii-keycloak

echo -e '\nKeycloak installation complete.'
