#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Stop and remove containers
#-----------------------------------------------------------------------------#

# removing current docker containers
docker rm -f $DOCKER_KEYCLOAK_CONTAINER_HOSTNAME || true;
echo;

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create $DOCKER_NETWORK_NAME || true;

#-----------------------------------------------------------------------------#
### Login to dockerhub
#-----------------------------------------------------------------------------#

set +x # Turning down verbosity as the password is echoed to stdout
docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
set -x
echo;

#-----------------------------------------------------------------------------#
### Keycloak Deployment
#-----------------------------------------------------------------------------#

docker run -dti \
--name $DOCKER_KEYCLOAK_CONTAINER_HOSTNAME \
-h $DOCKER_KEYCLOAK_CONTAINER_HOSTNAME \
-p $KEYCLOAK_PORT:8081 \
-v $BUNDLE_PARENT_PATH:$BUNDLE_PARENT_PATH \
--restart=always \
--network=$DOCKER_NETWORK_NAME \
$DOCKER_HUB_USERNAME/$DOCKER_HUB_REPO_NAME:$KEYCLOAK_IMAGE_TAG

#-----------------------------------------------------------------------------#
# set to not require ssl at login

sleep 30;

docker exec $DOCKER_KEYCLOAK_CONTAINER_HOSTNAME bash -c "
cd /root/keycloak-10.0.2/bin; \
./kcadm.sh config credentials \
--server http://localhost:8081/auth \
--realm master \
--user admin \
--password $KEYCLOAK_ADMIN_PASS; \
./kcadm.sh update \
realms/master \
-s sslRequired=NONE
"