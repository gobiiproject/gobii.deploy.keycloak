#!/bin/bash
function getKeycloakToken () {
	echo $(curl -d "username=$keycloakUser" -d "password=$keycloakUserPassword" -d "grant_type=$keycloakGrantType" -d "client_id=$keycloakClientId"    "$keycloakURL/auth/realms/master/protocol/openid-connect/token" | jq -r '.access_token')
}

function initializeKeyCloak () {
	keycloak_admin_token=$(getKeycloakToken)
	keycloak_realm_representation=$(cat $keycloakRealmRepresentationJSON)
	curl -H "Content-Type: application/json" -H "Authorization: Bearer $keycloak_admin_token" -d "$keycloak_realm_representation" $keycloakURL/auth/admin/realms/
	echo \n
	
}

function linkKeycloakUserWithApplicationUser() {
	if [[ ! -z $applicationUserTable && $keycloakDBModuleName == "postgresql" ]]
	then
		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "create sequence hibernate_sequence;"
		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "ALTER TABLE $dispatchSchema.\"$applicationUserTable\" ADD COLUMN \"KEYCLOAK_USER_ID\" VARCHAR(255);"
		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "ALTER TABLE $dispatchSchema.\"$applicationUserTable\" ADD UNIQUE (\"KEYCLOAK_USER_ID\");"
		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "ALTER TABLE $dispatchSchema.\"$applicationUserTable\" ADD CONSTRAINT \"FK_$applicationUserTable_USER_ENTITY\" FOREIGN KEY ( \"KEYCLOAK_USER_ID\" ) REFERENCES $keycloakSchema.\"user_entity\"( \"id\" ) ON DELETE CASCADE;"
	fi
}



while getopts ":u:w:G:C:K:J:T:B:D:U:P:S:s:" OPTION
do
	case $OPTION in
		u)
			keycloakUser=$OPTARG
			;;
		w)
			keycloakUserPassword=$OPTARG
			;;
		G)
			keycloakGrantType=$OPTARG
			;;
		C)
			keycloakClientId=$OPTARG
			;;
		K)
			keycloakURL=$OPTARG
			;;
		J)
			keycloakRealmRepresentationJSON=$OPTARG
			;;
		T)
			applicationUserTable=$OPTARG
			;;
		B)
			keycloakDBModuleName=$OPTARG
			;;
		D)
			keycloakDBName=$OPTARG
			;;
		U)
			keycloakDBUser=$OPTARG
			;;
		P)
			keycloakDBPassword=$OPTARG
			;;
		S)
			dispatchSchema=$OPTARG
			;;
		s)
			keycloakSchema=$OPTARG
			;;
	esac
done

tries=0
curl $keycloakURL/auth

while [[ "$?" == "7" && $tries -le 10 ]]
do
	echo "Waiting for keyclock server to start"
	sleep 5
	tries=$(( $tries + 1 ))
	curl $keycloakURL/auth
	
done

if [[  "$?" == "7" ]]
then
	echo "Failed to connect with keycloak server"
else
	echo "Connected to keycloak server successfully!"\n"Creating realms!"
	initializeKeyCloak
	echo "Linking Dispatch Application User to Keycloak User Entity"
	linkKeycloakUserWithApplicationUser
	echo "Done"
fi