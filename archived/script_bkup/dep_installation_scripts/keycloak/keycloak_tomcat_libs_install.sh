#!/bin/bash
sleep 10
sudo /opt/tomcat/bin/shutdown.sh
pushd $1/lib
sudo wget https://downloads.jboss.org/keycloak/7.0.1/adapters/keycloak-oidc/keycloak-tomcat-adapter-dist-7.0.1.tar.gz
sudo wget -O ojdbc8.jar https://repo1.maven.org/maven2/com/oracle/ojdbc/ojdbc8/19.3.0.0/ojdbc8-19.3.0.0.jar 
sudo tar -xzf keycloak-tomcat-adapter-dist-7.0.1.tar.gz
popd
sleep 10
sudo /opt/tomcat/bin/startup.sh