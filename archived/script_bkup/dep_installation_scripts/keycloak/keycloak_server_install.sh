#!/bin/bash
function usage()
{
  cat << EOF
  usage: $0 options

  OPTIONS:
    -h  Show this message
    -U  Name of database user (mandatory)
    -P  Passowrd for database user (mandatory)
    -D  Database name (mandatory)
    -S 	Schema in the database (optional)
    -d  Path to a dump file to load into the database (optional, but file must exist)
EOF
}

function installKeycloak()
{
	if [[ -z $keycloakInstallationPath ]]
	then
		usage
	else
		sudo apt-get install -y jq screen
		pushd $keycloakInstallationPath
		wget https://downloads.jboss.org/keycloak/7.0.0/keycloak-7.0.0.tar.gz
		tar -xzf keycloak-7.0.0.tar.gz
	fi
}

function generateKeycloakSchema()
{
	if [[ ! -z "$keycloakSchema" ]]
	then
		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "CREATE SCHEMA IF NOT EXISTS $keycloakSchema"
	fi
}

function configureKeycloak() {
	echo $keycloakDBModuleName
	echo $keycloakDBDriverDownloadUrl
	if [[ ! -z $keycloakDBModuleName && ! -z $keycloakDBDriverDownloadUrl ]]
	then
		echo $keycloakDBDriverDownloadUrl
		mkdir -p keycloak-7.0.0/modules/org/$keycloakDBModuleName/main	#Arg 2 is an alias for the DBMS that will be used
		wget -O keycloak-7.0.0/modules/org/$keycloakDBModuleName/main/$keycloakDBModuleName'_jdbc.jar' $keycloakDBDriverDownloadUrl #Arg 3 is for a url to the jdbc library for the DBMS to be used   
		sed -i 's/${jboss.http.port:8080}/${jboss.http.port:8081}/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		sed -i 's/${jboss.https.port:8443}/${jboss.https.port:8444}/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		if [[ $keycloakDBModuleName == "oracle" ]]
		then
			echo "We are working with oracle!!"
			sed -i 's/<connection-url>jdbc:h2:\${jboss\.server\.data\.dir}\/keycloak;AUTO_SERVER=TRUE<\/connection-url>/<connection-url>jdbc:'$keycloakDBModuleName':thin:@'$keycloakDBHost':'$keycloakDBPort'\/'$keycloakDBName'<\/connection-url>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		
		else
			sed -i 's/<connection-url>jdbc:h2:\${jboss\.server\.data\.dir}\/keycloak;AUTO_SERVER=TRUE<\/connection-url>/<connection-url>jdbc:'$keycloakDBModuleName':\/\/'$keycloakDBHost':'$keycloakDBPort'\/'$keycloakDBName'<\/connection-url>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		fi
		sed -i 's/<driver>h2<\/driver>/<driver>'$keycloakDBModuleName'<\/driver>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		sed -i 's/<user-name>sa<\/user-name>/<user-name>'$keycloakDBUser'<\/user-name>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		sed -i 's/<password>sa<\/password>/<password>'$keycloakDBPassword'<\/password>/g' keycloak-7.0.0/standalone/configuration/standalone.xml

		sed -i 's/<driver name="h2" module="com.h2database.h2">/<driver name="'$keycloakDBModuleName'" module="org.'$keycloakDBModuleName'">/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		sed -i 's/<xa-datasource-class>org.h2.jdbcx.JdbcDataSource<\/xa-datasource-class>/<xa-datasource-class>'$keycloakDBClass'<\/xa-datasource-class>/g' keycloak-7.0.0/standalone/configuration/standalone.xml

		sed -i 's/<default-bindings context-service="java:jboss\/ee\/concurrency\/context\/default" datasource="java:jboss\/datasources\/ExampleDS" managed-executor-service="java:jboss\/ee\/concurrency\/executor\/default" managed-scheduled-executor-service="java:jboss\/ee\/concurrency\/scheduler\/default" managed-thread-factory="java:jboss\/ee\/concurrency\/factory\/default"\/>/<default-bindings context-service="java:jboss\/ee\/concurrency\/context\/default" datasource="java:jboss\/datasources\/KeycloakDS" managed-executor-service="java:jboss\/ee\/concurrency\/executor\/default" managed-scheduled-executor-service="java:jboss\/ee\/concurrency\/scheduler\/default" managed-thread-factory="java:jboss\/ee\/concurrency\/factory\/default"\/>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		sed -i 's/<property name="migrationExport" value="${jboss.home.dir}\/keycloak-database-update.sql"\/>$/<property name="migrationExport" value="${jboss.home.dir}\/keycloak-database-update.sql"\/><property name="schema" value="'$keycloakSchema'"\/>/g' keycloak-7.0.0/standalone/configuration/standalone.xml
		echo '<?xml version="1.0" ?>
		<module xmlns="urn:jboss:module:1.3" name="org.'$keycloakDBModuleName'">

		    <resources>
		        <resource-root path="'$keycloakDBModuleName'_jdbc.jar"/>
		    </resources>

		    <dependencies>
		        <module name="javax.api"/>
		        <module name="javax.transaction.api"/>
		    </dependencies>
		</module>' > keycloak-7.0.0/modules/org/$keycloakDBModuleName/main/module.xml

		pushd keycloak-7.0.0/bin/
		./add-user-keycloak.sh -r master -u $keycloakUser -p $keycloakUserPassword
		screen -d -m -L -S keycloak-server ./standalone.sh -b=0.0.0.0
		popd
		popd
	fi
}

# function linkKeycloakUserWithApplicationUser() {
# 	if [[ ! -z $applicationUserTable && $keycloakDBModuleName == "postgresql" ]]
# 	then
# 		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "ALTER TABLE $keycloakSchema.\"$applicationUserTable\" ADD COLUMN \"KEYCLOAK_USER_ID\" VARCHAR(255);"
# 		PGPASSWORD=$keycloakDBPassword psql -U $keycloakDBUser -d $keycloakDBName  -c "ALTER TABLE $keycloakSchema.\"$applicationUserTable\" ADD CONSTRAINT \"FK_$applicationUserTable_USER_ENTITY\" FOREIGN KEY ( \"KEYCLOAK_USER_ID\" ) REFERENCES $keycloakSchema.\"user_entity\"( \"id\" );"
# 	fi
# }

while getopts ":C:K:s:B:X:H:p:D:U:P:u:w:T:" OPTION
do
	case $OPTION in
		C)
			keycloakDBClass=$OPTARG
			;;
		K)
			keycloakInstallationPath=$OPTARG
			;;
		s)
			keycloakSchema=$OPTARG
			;;
		B)
			keycloakDBModuleName=$OPTARG
			;;
		X)
			keycloakDBDriverDownloadUrl=$OPTARG
			;;
		H)
			keycloakDBHost=$OPTARG
			;;
		p)
			keycloakDBPort=$OPTARG
			;;
		D)
			keycloakDBName=$OPTARG
			;;
		U)
			keycloakDBUser=$OPTARG
			;;
		P)
			keycloakDBPassword=$OPTARG
			;;
		u)
			keycloakUser=$OPTARG
			;;
		w)
			keycloakUserPassword=$OPTARG
			;;
		T)
			applicationUserTable=$OPTARG
			;;
	esac
done

installKeycloak
generateKeycloakSchema
configureKeycloak
#linkKeycloakUserWithApplicationUser