#!/bin/bash
sudo apt-get update -y

if [[ -z "$(which psql)" ]]
then
  echo "installing postgres"
  sudo apt-get install postgresql -y
  echo "installing postgres extensions"
  sudo apt-get install postgresql-contrib -y
  sudo sed -i "s/local   all             postgres                                peer/local   all             postgres                                     peer\nlocal   all             all                                     md5/" /etc/postgresql/10/main/pg_hba.conf
  #best to tunnel
  sudo sed -i "s/host    all             all             127.0.0.1\/32            md5/host    all             all             0.0.0.0\/0            md5/" /etc/postgresql/10/main/pg_hba.conf
  sudo sed -i "s/#listen_addresses = 'localhost'/listen_addresses = '*'/" /etc/postgresql/10/main/postgresql.conf
  sudo service postgresql stop
  sudo -u postgres pg_ctlcluster 10 main start
  while [[ $? -ne 0 ]]
  do
  	echo "Waiting for database to die"
  	sleep 2
  	sudo -u postgres pg_ctlcluster 10 main start
  done
fi