FROM ubuntu:18.04

ENV keycloak_local_admin_user=admin
ENV keycloak_local_admin_password=@dm1np@55
ENV keycloak_db_schema=keycloak_schema
ENV home_location=/root

ENV dbms=postgresql
ENV dbms_jdbc_download_url=https://repo1.maven.org/maven2/org/postgresql/postgresql/42.2.8/postgresql-42.2.8.jar
ENV dbms_host=localhost
ENV dbms_port=5432
ENV dbms_db=keycloak_db
ENV dbms_db_user=keycloakdbadmin
ENV dbms_db_user_pw=k8yc10@#
ENV application_db_schema=gdm_schema
ENV keycloak_dbclass=org.postgresql.xa.PGXADataSource
ENV application_user_table=contacts


RUN apt-get update -y
RUN apt-get install -y locales locales-all tar
ENV LC_ALL=C.UTF-8
ENV LANG=C.UTF-8
RUN apt-get install -y sudo screen wget curl
RUN export DEBIAN_FRONTEND=noninteractive; apt-get install -y tzdata

COPY dep_installation_scripts /root/dep_installation_scripts
COPY dep_initialization_scripts /root/dep_initialization_scripts
COPY config.sh /root

RUN /root/dep_installation_scripts/java1.8/java8_install.sh
RUN /root/dep_installation_scripts/postgres/postgres_install.sh
RUN /root/dep_installation_scripts/keycloak/keycloak_server_install.sh -C $keycloak_dbclass -K $home_location -s $keycloak_db_schema -B $dbms -X $dbms_jdbc_download_url -H $dbms_host -p $dbms_port -D $dbms_db -U $dbms_db_user -P $dbms_db_user_pw -u $keycloak_local_admin_user -w $keycloak_local_admin_password

RUN chmod 755 /root/config.sh
ENTRYPOINT ["bash" , "-c", "/root/config.sh"]