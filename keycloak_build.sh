#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

#-----------------------------------------------------------------------------#
### Verify script runs with correct parameters
#-----------------------------------------------------------------------------#

if [ $# -lt 1 ];
  then
    echo "Arguments passed incorrect..."
    echo "Usage: bash keycloak_run.sh <Docker Image Tag>"
    exit 1
fi

#-----------------------------------------------------------------------------#
### Build Keycloak Image
#-----------------------------------------------------------------------------#

docker build . -t gadm01/gobii_keycloak:$1

#-----------------------------------------------------------------------------#
### Docker Login

set +x # Turning down verbosity as the password is echoed to stdout
echo;

#read -p "Please enter your Docker Hub Login Username: " DOCKER_HUB_LOGIN_USERNAME
#echo;

DOCKER_HUB_LOGIN_USERNAME="gadmreader"

read -sp "Please enter your Docker Hub password: " DOCKER_HUB_PASSWORD
echo;


docker login -u $DOCKER_HUB_LOGIN_USERNAME -p $DOCKER_HUB_PASSWORD;
echo;
set -x

#-----------------------------------------------------------------------------#
### Push Keycloak Image
#-----------------------------------------------------------------------------#

docker push gadm01/gobii_keycloak:$1
echo;

#-----------------------------------------------------------------------------#
### Script Completion...
#-----------------------------------------------------------------------------#

echo "Keycloak Build Completed"
echo;