# gobii.deploy.keycloak

This repository contains various methods of deploying Keycloak IAM that have been developed for use with GDM.

## How do I get set up?

The `docker_deploy_v2` directory contains the most recent method of deployment.  See the readme there for more information.
