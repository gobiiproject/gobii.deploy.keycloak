#!/bin/bash

#-----------------------------------------------------------------------------#
### Start postgres DB 
#-----------------------------------------------------------------------------#

service  postgresql start

#-----------------------------------------------------------------------------#
### Configure Keycloak DB 
#-----------------------------------------------------------------------------#

/root/dep_initialization_scripts/postgres/database_initializer.sh -U $dbms_db_user -P $dbms_db_user_pw -D $dbms_db -S $application_db_schema

/root/dep_initialization_scripts/keycloak/initialize.sh -u $keycloak_local_admin_user -w $keycloak_local_admin_password -G password -C admin-cli -K 'http://localhost:8081' -J /root/dep_initialization_scripts/keycloak/sample_realm_data.json -B $dbms -D $dbms_db -U $dbms_db_user -P $dbms_db_user_pw -S $application_db_schema -s $keycloak_db_schema -H $home_location

#-----------------------------------------------------------------------------#
### Set no ssl requirement default - master realm
#-----------------------------------------------------------------------------#

cd /root/keycloak-10.0.2/bin; ./kcadm.sh config credentials --server http://localhost:8081/auth --realm master --user $keycloak_local_admin_user --password $keycloak_local_admin_password; ./kcadm.sh update realms/master -s sslRequired=NONE;

#-----------------------------------------------------------------------------#
### Set no ssl requirement default - realm2 
#-----------------------------------------------------------------------------#

if [[ -v DOCKER_PORTAINER_VERSION ]]; then
cd /root/keycloak-10.0.2/bin; ./kcadm.sh config credentials --server http://localhost:8081/auth --realm $realm2 --user $realm2_user --password $realm2_pass; ./kcadm.sh update realms/$realm2 -s sslRequired=NONE;
fi

#-----------------------------------------------------------------------------#
### Entry Point - bash for persistent run time
#-----------------------------------------------------------------------------#

bash