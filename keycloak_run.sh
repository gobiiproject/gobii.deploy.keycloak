#!/usr/bin/env bash

#-----------------------------------------------------------------------------#
### Error on running with sudo
#-----------------------------------------------------------------------------#

if [[ $EUID -eq 0 ]]; then
    echo "This script should not be run using sudo or as the root user. Some commands will automatically prompt you if they need elevated permissions."
    exit 1
fi

if [ $# -lt 2 ];
  then
    echo "Arguments passed incorrect..."
    echo "Usage: bash keycloak_run.sh <Docker Image Tag> <External Image Port>"
    exit 1
fi

#-----------------------------------------------------------------------------#
### Creating gdm docker network
#-----------------------------------------------------------------------------#

# Creating and adding the gobii docker network for container internal 
# communication
docker network create gobii_net || true;

#-----------------------------------------------------------------------------#
### Keycloak Deployment
#-----------------------------------------------------------------------------#


docker run -dti \
--name keycloak-node \
-h keycloak-node \
-p $2:8081 \
--restart=always \
--network=gobii_net \
gadm01/gobii_keycloak:$1

